# tonnetz

A visual representation of musical intervals using the [Tonnetz](https://en.wikipedia.org/wiki/Tonnetz) concept.
[Demo](https://aheger.gitlab.io/tonnetz/)

# Project Goals

- Create a Tonnetz representation
- Learn about containerizing Rust apps
- Catch up with the current state of [yew](https://yew.rs/)

# Requirements

To run the development container either [podman](https://podman.io/) or [docker](https://www.docker.com/) are required.  
`podman` is recommended since `docker` is not used by the maintainer.

# Setup

Development is containerized.

**Build the development container**
```sh
scripts/dev build
```

**Set your environment variables**
```sh
cp .env.example .env
```
Each environment variable is described in the file.

# Usage

**Run the dev server**
```sh
scripts/dev trunk serve
```

**Build the application**
```sh
scripts/dev trunk build --release
```
