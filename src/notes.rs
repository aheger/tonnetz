use std::fmt;
use yew::html::{ Html, ToHtml };

#[derive(Clone, Copy, Debug, PartialEq)]
#[repr(u8)]
pub enum Notes {
	C,
	Db,
	D,
	Eb,
	E,
	F,
	Gb,
	G,
	Ab,
	A,
	Bb,
	B,
}

pub const NOTE_COUNT: u8 = 12;

pub static NOTE_VALUES: [Notes; NOTE_COUNT as usize] = [
	Notes::C,
	Notes::Db,
	Notes::D,
	Notes::Eb,
	Notes::E,
	Notes::F,
	Notes::Gb,
	Notes::G,
	Notes::Ab,
	Notes::A,
	Notes::Bb,
	Notes::B,
];

impl fmt::Display for Notes {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		let s = match self {
			Notes::C => "C",
			Notes::Db => "C#",
			Notes::D => "D",
			Notes::Eb => "D#",
			Notes::E => "E",
			Notes::F => "F",
			Notes::Gb => "F#",
			Notes::G => "G",
			Notes::Ab => "G#",
			Notes::A => "A",
			Notes::Bb => "A#",
			Notes::B => "B",
		};

		write!(f, "{}", s)
	}
}

impl ToHtml for Notes {
	fn to_html(&self) -> Html {
		return self.to_string().to_html();
	}
}
