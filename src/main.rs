mod cell;
mod cell_row;
mod notes;
mod ton_netz;

use yew::prelude::*;
use cell_row::CellRow;
use notes::{NOTE_COUNT, NOTE_VALUES, Notes};
use yew::use_state;
use wasm_bindgen::JsCast;
use web_sys::{EventTarget, HtmlInputElement, HtmlElement};

#[function_component]
fn App() -> Html {
	let root = use_state(|| Notes::C);

	let oninput = {
		let root = root.clone();
		Callback::from(move |e: InputEvent| {
			let target: Option<EventTarget> = e.target();
			let input = target
				.and_then(|t| t.dyn_into::<HtmlInputElement>().ok())
				.unwrap();

			root.set(NOTE_VALUES[input.value().parse::<u8>().unwrap() as usize]);
		})
	};

	let on_cell_click = {
		let root = root.clone();

		Callback::from(move |e: MouseEvent| {
			e.target()
				.and_then(|target| target.dyn_into::<HtmlElement>().ok())
				.and_then(|target_element| target_element.closest("tonnetz-cell").ok())
				.and_then(|target_cell_result| target_cell_result)
				.and_then(|target_cell| target_cell.get_attribute("data-interval"))
				.and_then(|interval_string| interval_string.parse::<u8>().ok())
				.and_then(|interval| {
					root.set(
						NOTE_VALUES[(*root as u8 + interval).rem_euclid(NOTE_COUNT) as usize],
					)
					.into()
				});
		})
	};

	html! {
		<>
			<form>
				<label>
					{"Root:"}
					<input
						type="range"
						min="0"
						max={(notes::NOTE_COUNT - 1).to_string()}
						{oninput}
						value={(*root as u8).to_string()}
					/> {*root}
				</label>
			</form>
			<tonnetz-container onclick={on_cell_click}>
			{ for ton_netz::get_netz(*root).into_iter().map(|notes| {
				html! {
					<CellRow {notes}/>
				}
			})}
			</tonnetz-container>
		</>
	}
}

fn main() {
	wasm_logger::init(wasm_logger::Config::default());
	log::info!("App starting.");
	yew::Renderer::<App>::new().render();
}
