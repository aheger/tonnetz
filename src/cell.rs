use crate::ton_netz::NetzNote;
use yew::{function_component, html, Html, Properties};

#[derive(PartialEq, Properties, Clone)]
pub struct CellProps {
	pub note: NetzNote,
}

#[function_component]
pub fn Cell(props: &CellProps) -> Html {
	html! {
		<tonnetz-cell data-interval={props.note.root_interval.to_string()}>
		{ props.note.note }
		</tonnetz-cell>
	}
}
