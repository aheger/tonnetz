use crate::notes::{Notes, NOTE_VALUES, NOTE_COUNT};

#[derive(Clone, Copy, Debug, PartialEq)]
pub struct NetzNote {
	pub note: Notes,
	pub root_interval: i8,
}

pub fn get_row(root_note: Notes, size: i8, offset: i8) -> Vec<NetzNote> {
	let mut result = Vec::new();

	// TODO simplify
	let mut cursor = (root_note as i8 + offset) - (size / 2) * 7;
	for _ in 0..size {
		let note_index = (cursor).rem_euclid(NOTE_COUNT as i8);

		let root_interval = (note_index - root_note as i8).rem_euclid(NOTE_COUNT as i8);

		result.push(NetzNote {
			note: NOTE_VALUES[note_index as usize],
			root_interval,
		});

		cursor += 7;
	}

	result
}

pub fn get_netz(root_note: Notes) -> Vec<Vec<NetzNote>> {
	vec![
		get_row(root_note, 11, -26),
		get_row(root_note, 10, -22),
		get_row(root_note, 11, -13),
		get_row(root_note, 10, -9),
		get_row(root_note, 11, 0),
		get_row(root_note, 10, 4),
		get_row(root_note, 11, 13),
		get_row(root_note, 10, 17),
		get_row(root_note, 11, 26),
	]
}
