use crate::cell::Cell;
use crate::ton_netz::NetzNote;
use yew::{function_component, html, Html, Properties};

#[derive(PartialEq, Properties, Clone)]
pub struct CellRowProps {
	pub notes: Vec<NetzNote>,
}

#[function_component]
pub fn CellRow(props: &CellRowProps) -> Html {
	html! {
		<tonnetz-cell-row>
		{ for props.notes.iter().map(|&note| {
			html! {
				<Cell {note} />
			}
		})}
		</tonnetz-cell-row>
	}
}
